<?php 
require_once "simplexls.php";
echo "Reading Spreadsheet file...";

    if ( $xlsx = SimpleXLSX::parse('users.xlsx') ) {
        echo '<table><tbody>';
        $i = 0;

        foreach ($xlsx->rows() as $elt) {
        if ($i == 0) {
            // echo "<tr><th>" . $elt[0] . "</th><th>" . $elt[1] . "</th></tr>";
        } else {
            $full_name = $elt[0];
            $email = $elt[1];
            $first_name = get_first_name($full_name);
            $last_name = get_last_name($full_name);
            $username = get_username_from_email($email);
            // Create the User
            $exec = "wp user create $username $email --role=referee --first_name=$first_name --last_name=$last_name --send-email --porcelain";
            shell_exec($exec);
        }      

        $i++;
        }

        echo "Done!";

    } else {
        echo SimpleXLSX::parseError();
    }

    function get_first_name($full_name) {
        $name = trim($full_name);
        $first_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $last_name = trim( preg_replace('#'.$first_name.'#', '', $name ) );
        return $first_name;
    }

    function get_last_name($full_name) {
        $name = trim($full_name);
        $first_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $last_name = trim( preg_replace('#'.$first_name.'#', '', $name ) );
        return $last_name;
    }

    function get_username_from_email($email) {
        $group = $email = explode("@", $email);
        return $group[0];
    }
?>